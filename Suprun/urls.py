"""Suprun URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
''''
from django.conf.urls import url, include, paterns
from django.contrib import admin
from app.forms import BootstrapAuthenticationForm

urlpatterns = [
    url(r'^admin/', admin.site.urls),
]
'''

from datetime import datetime
from django.conf.urls import patterns, url, include
from app.forms import BootstrapAuthenticationForm
from django.conf import settings
from django.conf.urls.static import static

# Uncomment the next lines to enable the admin:
 #from django.conf.urls import include
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^$','app.views.home'),
    url(r'^myapp$', 'app.views.home', name='home'),
    url(r'^myapp/contact$', 'app.views.contact', name='contact'),
    url(r'^myapp/about', 'app.views.about', name='about'),
    url(r'^login/$',
        'django.contrib.auth.views.login',
        {
            'template_name': 'app/login.html',
            'authentication_form': BootstrapAuthenticationForm,
            'extra_context':
            {
                'title':'Log in',
                'year':datetime.now().year,
            }
        },
        name='login'),
    url(r'^myapp/logout$',
        'django.contrib.auth.views.logout',
        {
            'next_page': '/myapp',
        },
        name='logout'),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
     url(r'^admin/', include(admin.site.urls)),
     #url(r'^$/', include("app.urls")),
     url(r'^myapp/create/$', "app.views.post_create", name="create"),
     url(r'^myapp/(?P<id>\d+)/$', "app.views.post_detail", name="detail"),
     url(r'^myapp/(?P<id>\d+)/edit/$', "app.views.post_update", name="update"),
     url(r'^myapp/(?P<id>\d+)/delete/$', "app.views.post_delete", name="delete"),
     url(r'^myapp/', include('app.urls')),
)

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
