from rest_framework import serializers
from app.models import Post
from django.contrib.auth.models import User


class PostSerializer(serializers.HyperlinkedModelSerializer):
	class Meta:
		model = Post
		fields = ('id','title', 'author', 'image','timestamp','flag','likes','dislikes')