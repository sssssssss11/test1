# Create your views here.
from django.shortcuts import render, get_object_or_404,render_to_response, redirect
from django.http import HttpRequest, HttpResponse, HttpResponseRedirect
from django.template import RequestContext
from datetime import datetime
from django.contrib import messages
from django.core import serializers
from app.serializers import PostSerializer
from .permissions import IsOwnerOrReadOnly
from rest_framework import viewsets


import simplejson as json
from .forms import PostForm
from .models import Post

def home(request):
    """Renders the home page."""
    queryset = Post.objects.all()
    context = {
        'objects_list':queryset,
        'title':'Home Page',
        'year':datetime.now().year,
    }
    assert isinstance(request, HttpRequest)
    '''
    return render(
        request,
        'app/index.html',
        context_instance = RequestContext(request,
        {
            'title':'Home Page',
            'year':datetime.now().year,
        })
    )
    '''
    return render(request, 'app/index.html',context)

def contact(request):
    """Renders the contact page."""
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'app/contact.html',
        context_instance = RequestContext(request,
        {
            'title':'Contact',
            'message':'Your contact page.',
            'year':datetime.now().year,
        })
    )

def about(request):
    """Renders the about page."""
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'app/about.html',
        context_instance = RequestContext(request,
        {
            'title':'About',
            'message':'Your application description page.',
            'year':datetime.now().year,
        })
    )


def post_create(request):
    form = PostForm(request.POST or None, request.FILES or None)
    if form.is_valid():
        instance = form.save(commit=False)
        instance.save()
        messages.success(request,"Successefully created")
        return HttpResponseRedirect(instance.get_absolute_url())
    else:
        messages.success(request,"Not Successefully created")

    context = {
        "form": form,
    }
    return render(request, 'app/post_form.html', context)

def post_detail(request, id=None):
    instance = get_object_or_404(Post, id=id)
    context = {
        "title": instance.title,
        "instance": instance,
    }
    return render(request,"app/post_detail.html",context)


def post_update(request,id=None):
    instance = get_object_or_404(Post, id=id)
    form = PostForm(request.POST or None,instance=instance)
    if form.is_valid():
        instance = form.save(commit=False)
        instance.save()
        messages.success(request,"Successefully updated")
        return HttpResponseRedirect(instance.get_absolute_url())

    context = {
        "title": instance.title,
        "instance": instance,
        "form":form,
    }
    return render(request, "app/post_form.html", context)

def post_delete(request,id=None):
    instance = get_object_or_404(Post, id=id)
    instance.delete()
    return redirect("home")


class PostViewSet(viewsets.ModelViewSet):
        queryset = Post.objects.all()
        serializer_class = PostSerializer
        permission_classes = (IsOwnerOrReadOnly,)
    

        def pre_safe(self, obj):
            obj.owner = self.request.user