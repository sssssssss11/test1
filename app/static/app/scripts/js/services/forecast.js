app.factory('forecast', ['$http', function($http) { 
  return $http.get('http://127.0.0.1:8000/myapp/api/myapp.json') 
            .success(function(data) { 
              return data; 
            }) 
            .error(function(err) { 
              return err; 
            }); 
}]);