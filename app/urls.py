from django.conf.urls import *
from rest_framework import routers
from app import views

router = routers.DefaultRouter()
router.register(r'myapp', views.PostViewSet)

urlpatterns = patterns('',
						url(r'^api/', include(router.urls)),
						)