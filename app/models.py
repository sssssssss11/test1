from __future__ import unicode_literals
from django.db import models
from django.core.urlresolvers import reverse

# Create your models here.
def upload_location(instance, filename):
	return "%s/%s" %(instance.id,filename)

class Post(models.Model):
	title = models.CharField(max_length=120)
	author = models.CharField(max_length=60)
	isbn = models.CharField(max_length=20 ,unique=True )
	image = models.ImageField(upload_to=upload_location, 
				null=True, 
				blank=True,  
				height_field="heigth_field",
				width_field="width_field")
	heigth_field = models.IntegerField(default=0)
	width_field = models.IntegerField(default=0)
	flag = models.BooleanField(default=True)
	updated = models.DateTimeField(auto_now=True, auto_now_add=False)
	timestamp = models.DateTimeField(auto_now=False, auto_now_add=True)
	likes = models.IntegerField(default=0)
	dislikes = models.IntegerField(default=0)

	def __unicode__(self):
		return self.title

	def __str__(self):
		return self.title

	def get_absolute_url(self):
		return reverse("detail", kwargs={"id": self.id})

	def as_dict(self):
		return {
			'id': self.id,
			'title': self.title,
			'author': self.author,
			'isbn': self.isbn,
			'image': self.image,
			'timestamp': self.timestamp
			}