Django==1.9.4
django-angular==0.8.1
djangorestframework==3.3.3
Pillow==3.2.0
simplejson==3.8.2
